/* jshint node: true */
'use strict';

var HtmlDriver = require('./html');

/**
  # driven

  Driven is an experimental library the embraces the concept of Model Driven
  Views, using the excellent [Gedi](https://github.com/gaffa-tape/gedi) and
  [Gel](https://github.com/KoryNunn/gel) libraries.

  ## Example Use

  <<< demo/index.js

**/
var driven = module.exports = function(data) {
  return new HtmlDriver(data);
};

/* helpers */

driven.scope = require('./scope');