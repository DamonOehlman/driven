# driven

Driven is an experimental library the embraces the concept of Model Driven
Views, using the excellent [Gedi](https://github.com/gaffa-tape/gedi) and
[Gel](https://github.com/KoryNunn/gel) libraries.


[![NPM](https://nodei.co/npm/driven.png)](https://nodei.co/npm/driven/)

![experimental](https://img.shields.io/badge/stability-experimental-red.svg)

## Example Use

```js
var driven = require('driven');
var Gedi = require('gedi');
var randomName = require('random-name');
var baseData = window.baseData = {
  name: randomName(),
  happy: true,

  friends: [
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() }
  ]
};

// create a gedi instance (optional, not required)
var data = new Gedi(baseData);

// drive the base data
var driver = driven(data);

function updateName() {
  data.set('[/name]', randomName());
}

function updateFriend() {
  var friendIndex = Math.random() * data.get('(length [/friends])');

  data.set('[/friends/' + (friendIndex|0) + '/name]', randomName());
}

window.addEventListener('load', function() {
  updateName();
  setInterval(updateName, 250);
  setInterval(updateFriend, 50);
});


```

#### remove(target)

Debind listening on the node

## License(s)

### MIT

Copyright (c) 2014 Damon Oehlman <damon.oehlman@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
