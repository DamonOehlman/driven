var paths = require('gedi-paths');

module.exports = function(data) {
  var basePath = paths.create();

  return {
    get: function(name, path) {
      return data.get(name, paths.append(basePath, path));
    },

    set: function(name, value, path, notify) {
      if (typeof name == 'object' && (! (name instanceof String))) {
        return data.set(name, paths.append(basePath, path), notify);
      }
      else {
        return data.set(name, value, paths.append(basePath, path), notify);
      }
    },

    bind: function(path, handler) {
      return data.bind(paths.append(basePath, path), handler);
    },

    debind: function(path, handler) {
      if (typeof path == 'function') {
        return data.debind(path);
      }
      else {
        return data.debind(paths.append(basePath, path), handler);
      }
    }
  }
};