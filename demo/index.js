var driven = require('../');
var Gedi = require('gedi');
var randomName = require('random-name');
var baseData = window.baseData = {
  name: randomName(),
  happy: true,

  friends: [
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() },
    { name: randomName() }
  ]
};

// create a gedi instance (optional, not required)
var data = new Gedi(baseData);

// drive the base data
var driver = driven(data);

function updateName() {
  data.set('[/name]', randomName());
}

function updateFriend() {
  var friendIndex = Math.random() * data.get('(length [/friends])');

  data.set('[/friends/' + (friendIndex|0) + '/name]', randomName());
}

window.addEventListener('load', function() {
  updateName();
  setInterval(updateName, 250);
  setInterval(updateFriend, 50);
});

