module.exports = function(el, data, binding, path) {
  // set the initial value
  el.innerHTML = data.get(binding, path) || '';

  // listen for binding changes
  data.bind(binding, function(evt) {
    el.innerHTML = data.get(binding, path) || '';
  }, path);
};