module.exports = function(el, data, binding, path) {
  var prop = 'value';
  var evtType = 'input';

  // tweak property names and event types based on the type of
  // input element
  switch (el.type) {
    case 'checkbox': {
      prop = 'checked';
      evtType = 'click';

      break;
    }
  }

  // set the initial value
  el[prop] = data.get(binding, path);

  // listen for binding changes
  data.bind(binding, function(evt) {
    // update the value
    if (el[prop] !== evt.getValue()) {
      el[prop] = evt.getValue();
    }
  }, path);

  // add a listener to the element to listen for data updates
  el.addEventListener(evtType, function(evt) {
    data.set(binding, el[prop], path);
  });
};