/* jshint node: true */
'use strict';

var scope = require('../scope');
var paths = require('gedi-paths');

module.exports = function(el, data, binding, basePath) {
  var driver = this;
  var nodes = [];
  var itemCount;

  function syncDOM(evt) {
    var value = evt && typeof evt.getValue == 'function' && evt.getValue();
    var ii;
    var count;
    var node;
    var path;

    // ignore updates to children of the current list
    if (value && value.length === itemCount) {
      return;
    }

    // initialise value and path
    // value = data.get(binding, basePath);
    path = paths.append(basePath, binding);

    // if we have an array, then create subelements
    if (Array.isArray(value)) {
      el.style.display = 'none';

      // remove the parentNode from the dom

      // remove the nodes
      nodes.splice(0).forEach(function(oldNode) {
        driver.remove(oldNode);
        el.parentNode.removeChild(oldNode);
      });

      // clone the node
      for (ii = 0, itemCount = count = value.length; ii < count; ii++) {
        nodes[nodes.length] = node = el.cloneNode(true);
        node.removeAttribute('data-binding');
        node.setAttribute('data-path', paths.append(path, paths.create(ii)));
        node.style.display = 'inherit';

        // remove the binding attribute
        driver.attach(node);

        // add the new node to the dom
        el.parentNode.insertBefore(node, el);
      }

      return false;
    }
    else {
      el.style.display = 'inherit';
    }
  }

  // listen for binding changes
  data.bind(binding, syncDOM, basePath);

  return syncDOM();
};